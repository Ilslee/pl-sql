CREATE OR REPLACE function STUDENT.change_color(p_color varchar2) return varchar2 is
    cursor c1 (a_color varchar2) is
        select (case when r <= 50 then 0 else r - 50 end) r,
               g,
               (case when b <= 150 then 0 else b - 150 end) b
        from (select max(case when substr(color, 0, 1) = 'r' then substr(color, 2) end) r,
                     max(case when substr(color, 0, 1) = 'g' then substr(color, 2) end) g,
                     max(case when substr(color, 0, 1) = 'b' then substr(color, 2) end) b
              from (select regexp_substr(a_color, '[r][0-9]+') color
                    from dual
                    union all
                    select regexp_substr(a_color, '[g][0-9]+') color
                    from dual
                    union all
                    select regexp_substr(a_color, '[b][0-9]+') color
                    from dual));
    v_rgb varchar2(255);
    v_red varchar2(25);
    v_green varchar2(25);
    v_blue varchar2(25);
begin
    open c1(p_color);
    fetch c1 into v_red, v_green, v_blue;
    close c1;
    v_rgb := 'r' || v_red || 'g' || v_green || 'b' || v_blue;
    return v_rgb;
end;
/