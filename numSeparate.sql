declare
    v_src number := '432';
    v_digit number;
    v_digit_length number;
    v_loop number := 0;
    v_num_char varchar2(255) := '';
begin
    v_digit_length := (length(v_src))/2;
    for v_loop in 0..v_digit_length loop
        v_digit := regexp_substr(v_src, '^[0-9]{1,2}');
        if v_digit is null then
            exit;
        else
            v_src := regexp_replace(v_src, '^[0-9]{1,2}', '');
            v_num_char := v_num_char || v_digit || '.';
        end if;
    end loop;
    v_num_char := rtrim(v_num_char, '.');
    dbms_output.put_line(v_num_char);
end;