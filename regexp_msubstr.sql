create or replace function regexp_msubstr(v_stroke in varchar2,
                                          v_ahead in  varchar2,
                                          v_pattern in varchar2)
                                          return varchar2
is
    --stroke varchar2(255) := 'DEUTSHE BANK  TRUST COMPANY  AMERICAS,NEW YORK acc 04413152 SWIFT code -BKTRUS33';
    v_parsed_stroke varchar2(255);
    --pattern varchar2(255) := '.*';
    --ahead varchar2(255) := '((s|S)(wift|WIFT)(.*(:|-)|\s))';
begin
    select regexp_substr(v_stroke, v_ahead||v_pattern)
    into v_parsed_stroke
    from dual;
    --dbms_output.put_line(v_parsed_stroke);
    select regexp_replace(v_parsed_stroke, v_ahead, '')
    into v_parsed_stroke
    from dual;
    --dbms_output.put_line(parsed_stroke);
    return v_parsed_stroke;
end;